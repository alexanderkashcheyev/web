import { CriteriaService } from './ahr/criteria.service';
import { AlternativeService } from './ahr/alternative.service';
import { Component } from '@angular/core';
import { Criteria } from './ahr/criteria';

@Component({
  selector: 'eahr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'eahr';

  constructor(private alternativeService: AlternativeService,
    private criteriaService: CriteriaService) {
  }

  criteriasOk(): boolean {
    return this.criteriaService.isTransitive();
  }

  alternativesOk(): boolean {
    let res = true;
    this.criteriaService.criterias.forEach((c: Criteria) => {
      res = res && this.alternativeService.isDataCorrect(c);
    });
    return res;
  }
}
