import { SharedModule } from './shared/shared.module';
import { ResultComponent } from './ahr/result/result.component';
import { AlternativeListComponent } from './ahr/alternative-list/alternative-list.component';
import { AlternativeComparisonComponent } from './ahr/alternative-comparison/alternative-comparison.component';
import { CriteriaListComponent } from './ahr/criteria-list/criteria-list.component';
import { AhrModule } from './ahr/ahr.module';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

const appRoutes: Routes = [
  {
    path: 'criteria',
    component: CriteriaListComponent
  },
  {
    path: 'alternative',
    component: AlternativeListComponent
  },
  {
    path: 'comparison',
    component: AlternativeComparisonComponent
  },
  {
    path: 'result',
    component: ResultComponent
  },
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AhrModule,
    SharedModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
