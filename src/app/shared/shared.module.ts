import { L10nComponent } from './l10n/l10n.component';
import { L10nService } from './l10n/l10n.serivice';
import { FormsModule } from '@angular/forms';
import { NgModule, ChangeDetectorRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListComponent } from './list/list.component';
import { ComparatorComponent } from './comparator/comparator.component';
import { TranslatePipe } from './l10n/translate.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    ListComponent,
    ComparatorComponent,
    L10nComponent,
    TranslatePipe
  ],
  declarations: [ListComponent, ComparatorComponent, TranslatePipe, L10nComponent],
  providers: [L10nService]
})
export class SharedModule { }
