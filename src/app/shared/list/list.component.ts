import { Listable } from './../listable';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'eahr-list',
  templateUrl: './list.component.html'
})
export class ListComponent implements OnInit {

  @Input() list: Array<Listable>;

  @Output() onAdd = new EventEmitter<string>();
  @Output() onDelete = new EventEmitter<Listable>();

  private newName: string;

  constructor() { }

  ngOnInit() {
  }

  private delete(l: Listable): void {
    this.onDelete.emit(l);
  }

  private add(): void {
    if (this.newName.trim() !== '') {
      this.onAdd.emit(this.newName);
    }
    this.newName = '';
  }

}
