import { Caption } from './../caption';
import { Comparison } from './../comparison';
import { Listable } from './../listable';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'eahr-comparator',
  templateUrl: './comparator.component.html'
})
export class ComparatorComponent implements OnInit {

  @Input() list: Listable[];
  @Input() comparisons: Map<Listable, Map<Listable, number>>;
  @Input() captions: Caption[];

  @Output() onChange = new EventEmitter<Comparison<Listable>>();

  constructor() { }

  ngOnInit() {
  }

  change(c1: Listable, c2: Listable, value: number) {
    this.onChange.emit({
      one: c1,
      two: c2,
      value: value
    });
  }

}
