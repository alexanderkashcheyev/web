import { Listable } from './listable';
export class Comparator<T> {
    public comparisons: Map<T, Map<T, number>> =
        new Map<T, Map<T, number>>();

    public setComparison(c1: T, c2: T, value: number, invert: boolean = true) {
        if (!this.comparisons.has(c1)) {
            this.comparisons.set(c1, new Map<T, number>());
        }
        this.comparisons.get(c1).set(c2, value);

        if (invert) {
            this.setComparison(c2, c1, value == 1 ? 1 : -value, false);
        }
    }

    public isTransitive(): boolean {
        const comparees = new Array<T>();

        this.comparisons.forEach((v: any, k: T) => {
            comparees.push(k);
        });

        if (comparees.length === 0) {
            return null;
        }

        let res = true;
        comparees.forEach((a: T) => {
            if (!res) {
                return;
            }
            comparees.forEach((b: T) => {
                if (!res) {
                    return;
                }
                comparees.forEach((c: T) => {
                    if (a !== b && b !== c && c !== a) {
                        if (
                            (
                                this.comparisons.get(a).get(b) >= 1
                                && this.comparisons.get(b).get(c) >= 1
                                && this.comparisons.get(a).get(c) < 1
                            )
                        ) {
                            res = false;
                            return;
                        }
                    }
                });
            });
        });
        return res;
    }

    public purge(key: T): void {
        this.comparisons.delete(key);
        this.comparisons.forEach((v: Map<T, number>) => {
            v.delete(key);
        });
    }

    public getWeights(): Map<T, number> {
        const res = new Map<T, number>();
        const eigenvectors = new Map<T, number>();

        let sum = 0;

        this.comparisons.forEach((v: Map<T, number>, k: T) => {
            let composition = 1;

            v.forEach((v1: number) => {
                composition *= this.convertRating(v1);
            });

            const eigenvector = Math.pow(composition, 1 / v.size);
            eigenvectors.set(k, eigenvector);
            sum += eigenvector;
        });

        eigenvectors.forEach((v: number, k: T) => {
            res.set(k, v / sum);
        });

        return res;
    }

    private convertRating(n: number): number {
        if (n < 0) {
            return -1 / n;
        } else {
            return n;
        }
    }
}

