import { L10n } from './l10n';
import { Injectable } from '@angular/core';
import { EN } from './en';
import { RU } from './ru';

@Injectable()
export class L10nService {
    public l10ns: Map<string, L10n> = new Map<string, L10n>();

    private lang: string;

    constructor() {
        this.l10ns.set('ru', RU);
        this.language = 'ru';
    }

    public set language(lang: string) {
        if (this.l10ns.has(lang)) {
            this.lang = lang;
        } else {
            this.lang = 'en';
        }
    }

    public get language(): string {
        return this.lang;
    }

    public getL10n(): L10n {
        return this.l10ns.get(this.lang);
    }

    public t(key: string, base = this.getL10n()): string {
        const firstDot = key.indexOf('.');
        if (firstDot < 0) {
            return base[key];
        } else {
            return this.t(key.substring(firstDot + 1), base[key.substring(0, firstDot)]);
        }
    }
}
