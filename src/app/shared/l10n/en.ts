import { L10n } from './l10n';

export const EN: L10n = {
    code: 'en',
    langName: 'English',
    list: {
        addButton: 'Add',
        addPlaceholder: 'New element'
    },
    tabTitles: {
        criterias: 'Criterias',
        alternatives: 'Alternatives',
        comparisons: 'Comparisons',
        result: 'Result'
    },
    prompts: {
        criterias: 'Define criterias for optimal choice',
        compareCriterias: 'Compare defined criterias by their importancy',
        alternatives: 'Enter alternative you wish to choose from',
        compareAlternatives: 'Compare the alternatives by the criterias',
        compareBy: 'Compare by',
        rating: 'By given criterias, here is the alternatives rating'
    },
    compare: {
        leftPrefix: '',
        leftPostfix: 'is',
        rightPrefix: 'than',
        rightPostfix: '',
    },
    compareOptions: {
        importancy: [
            {value: -9, caption: 'excessively less important'},
            {value: -7, caption: 'strongly less important'},
            {value: -5, caption: 'significantly less important'},
            {value: -3, caption: 'slightly less important'},
            {value: 1, caption: 'equally important'},
            {value: 3, caption: 'slightly more important'},
            {value: 5, caption: 'significantly more important'},
            {value: 7, caption: 'strongly more important'},
            {value: 9, caption: 'excessively more important'}
        ],
        fitness: [
            {value: -9, caption: 'excessively worse'},
            {value: -7, caption: 'strongly worse'},
            {value: -5, caption: 'significantly worse'},
            {value: -3, caption: 'slightly worse'},
            {value: 1, caption: 'equally good'},
            {value: 3, caption: 'slightly better'},
            {value: 5, caption: 'significantly better'},
            {value: 7, caption: 'strongly better'},
            {value: 9, caption: 'excessively better'}
        ],
    },
    alerts: {
        criteria: {
            ok: 'The data you entered looks correct.',
            error: 'The criterias are compared in incorrect way. Please check comparisons.'
        },
        alternative: {
            ok: 'The data you entered looks correct.',
            error: 'The alternatives are compared in an incorrect way. Please check comparisons'
        }
    },
};
