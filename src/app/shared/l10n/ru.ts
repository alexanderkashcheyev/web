import { L10n } from './l10n';

export const RU: L10n = {
    code: 'ru',
    langName: 'Русский',
    list: {
        addButton: 'Добавить',
        addPlaceholder: 'Новый элемент'
    },
    tabTitles: {
        criterias: 'Критерии',
        alternatives: 'Альтернативы',
        comparisons: 'Сравнения',
        result: 'Результат'
    },
    prompts: {
        criterias: 'Определите необходимые критерии',
        compareCriterias: 'Сравните критерии по их важности',
        alternatives: 'Введите альтернативы, из которых собираетесь выбирать',
        compareAlternatives: 'Сравните альтернативы по выбранным критериям',
        compareBy: 'По критерию',
        rating: 'Альтернативы отсортированы от наиболее подходящего к наименее подходящему'
    },
    compare: {
        leftPrefix: '',
        leftPostfix: '',
        rightPrefix: '',
        rightPostfix: '',
    },
    compareOptions: {
        importancy: [
            {value: -9, caption: 'несравнимо менее важно, чем'},
            {value: -7, caption: 'намного менее важно, чем'},
            {value: -5, caption: 'значительно менее важно, чем'},
            {value: -3, caption: 'умеренно менее важно, чем'},
            {value: 1, caption: 'так же важно, как'},
            {value: 3, caption: 'умеренно важнее, чем'},
            {value: 5, caption: 'значительно важнее, чем'},
            {value: 7, caption: 'намного важнее, чем'},
            {value: 9, caption: 'несравнимо важнее, чем'}
        ],
        fitness: [
            {value: -9, caption: 'несравнимо хуже, чем'},
            {value: -7, caption: 'намного хуже, чем'},
            {value: -5, caption: 'значительно хуже, чем'},
            {value: -3, caption: 'умеренно хуже, чем'},
            {value: 1, caption: 'так же хорошо, как'},
            {value: 3, caption: 'немного лучше, чем'},
            {value: 5, caption: 'значительно лучше, чем'},
            {value: 7, caption: 'намного лучше, чем'},
            {value: 9, caption: 'несравнимо лучше, чем'}
        ],
    },
    alerts: {
        criteria: {
            ok: 'Введенные данные выглядят правильно.',
            error: 'Сравнения не сходятся. Пожалуйста, проверьте.'
        },
        alternative: {
            ok: 'Введенные данные выглядят правильно.',
            error: 'Сравнения не сходятся. Пожалуйста, проверьте.'
        }
    },
};
