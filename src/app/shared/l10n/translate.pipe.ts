import { L10nService } from './l10n.serivice';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translate'
})
export class TranslatePipe implements PipeTransform {

  constructor(private l10nService: L10nService) {}

  transform(value: string): any {
    return this.l10nService.t(value);
  }

}
