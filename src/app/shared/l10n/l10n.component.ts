import { element } from 'protractor';
import { L10nService } from './l10n.serivice';
import { Component, ChangeDetectorRef } from '@angular/core';
import { L10n } from './l10n';

@Component({
    selector: 'eahr-l10n',
    templateUrl: './l10n.component.html'
})
export class L10nComponent {
    constructor(private l10nService: L10nService, private cdRef: ChangeDetectorRef) {}

    langs(): L10n[] {
        const res = new Array<L10n>();
        this.l10nService.l10ns.forEach((v: L10n) => {
            res.push(v);
        });
        return res;
    }

    setLang(lang: string): void {
        this.l10nService.language = lang;
        this.cdRef.detectChanges();
    }

    currentLang(): string {
        return this.l10nService.language;
    }
}
