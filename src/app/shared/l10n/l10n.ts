import { Caption } from './../caption';
export class L10n {
    code: string;
    langName: string;
    list: {
        addButton: string,
        addPlaceholder: string
    };
    tabTitles: {
        criterias: string,
        alternatives: string,
        comparisons: string,
        result: string
    };
    prompts: {
        criterias: string,
        compareCriterias: string,
        alternatives: string,
        compareAlternatives: string,
        compareBy: string,
        rating: string
    };
    compare: {
        leftPrefix: string,
        leftPostfix: string,
        rightPrefix: string,
        rightPostfix: string,
    };
    compareOptions: {
        importancy: Array<Caption>;
        fitness: Array<Caption>;
    };
    alerts: {
        criteria: {
            ok: string,
            error: string
        },
        alternative: {
            ok: string,
            error: string
        }
    };
}
