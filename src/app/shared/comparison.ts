export class Comparison<T> {
    public one: T;
    public two: T;
    public value: number;
}
