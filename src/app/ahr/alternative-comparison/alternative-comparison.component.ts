import { L10nService } from './../../shared/l10n/l10n.serivice';
import { Caption } from './../../shared/caption';
import { Listable } from './../../shared/listable';
import { Comparison } from './../../shared/comparison';
import { CriteriaService } from './../criteria.service';
import { AlternativeService } from './../alternative.service';
import { Criteria } from './../criteria';
import { Alternative } from './../alternative';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'eahr-alternative-comparison',
  templateUrl: './alternative-comparison.component.html'
})
export class AlternativeComparisonComponent implements OnInit {

  constructor(
    private alternativeService: AlternativeService,
    private criteriaService: CriteriaService,
    private l10nService: L10nService
  ) {
  }

  ngOnInit() {
  }

  get captions(): Caption[] {
    return this.l10nService.getL10n().compareOptions.fitness;
  }

  criterias(): Criteria[] {
    return this.criteriaService.criterias;
  }

  alternatives(): Alternative[] {
    return this.alternativeService.alternatives;
  }

  comparisons(c: Criteria): Map<Listable, Map<Listable, number>> {
    return this.alternativeService.getComparisons(c);
  }

  setComparison(criteria: Criteria, comparison: Comparison<Alternative>): void {
    this.alternativeService.setComparison(criteria, comparison.one, comparison.two, comparison.value);
  }

  isDataCorrect(c: Criteria) {
    return this.alternativeService.isDataCorrect(c);
  }

  isAllDataCorrect(): boolean {
    let res = true;
    this.criterias().forEach((c: Criteria) => {
      res = res && this.isDataCorrect(c);
    });
    return res;
  }
}
