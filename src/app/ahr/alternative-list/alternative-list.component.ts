import { Criteria } from './../criteria';
import { AlternativeService } from './../alternative.service';
import { Component, OnInit } from '@angular/core';
import { Alternative } from './../alternative';

@Component({
  selector: 'eahr-alternative-list',
  templateUrl: './alternative-list.component.html',
})
export class AlternativeListComponent implements OnInit {

  constructor(private alternativeService: AlternativeService) { }

  ngOnInit() {
  }

  addAlternative(name: string): void {
    this.alternativeService.addAlternative(name);
  }

  deleteAlternative(a: Alternative): void {
    this.alternativeService.deleteAlternative(a);
  }

  alternatives(): Alternative[] {
    return this.alternativeService.alternatives;
  }
}
