import { L10nService } from './../../shared/l10n/l10n.serivice';
import { Caption } from './../../shared/caption';
import { Comparison } from './../../shared/comparison';
import { CriteriaService } from './../criteria.service';
import { Criteria } from './../criteria';
import { NgModel } from '@angular/forms';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'eahr-criteria-list',
  templateUrl: './criteria-list.component.html'
})
export class CriteriaListComponent implements OnInit {

  newCriteriaName = '';

  constructor(private criteriaService: CriteriaService, private l10nService: L10nService) {
  }

  ngOnInit() {
  }

  get captions(): Caption[] {
    return this.l10nService.getL10n().compareOptions.importancy;
  }

  criterias(): Criteria[] {
    return this.criteriaService.criterias;
  }

  comparisons(): Map<Criteria, Map<Criteria, number>> {
    return this.criteriaService.getComparisons();
  }

  addCriteria(name: string): void {
    this.criteriaService.addCriteria(name);
  }

  deleteCriteria(c: Criteria): void {
    this.criteriaService.deleteCriteria(c);
  }

  setComparison(comparison: Comparison<Criteria>) {
    this.criteriaService.setComparison(comparison.one, comparison.two, comparison.value);
  }

  isDataCorrect(): boolean {
    return this.criteriaService.isTransitive();
  }
}
