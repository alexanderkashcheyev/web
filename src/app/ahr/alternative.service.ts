import { CriteriaService } from './criteria.service';
import { Criteria } from './criteria';
import { Comparator } from './../shared/comparator';
import { Comparison } from './../shared/comparison';
import { Alternative } from './alternative';
import { Injectable } from '@angular/core';

@Injectable()
export class AlternativeService {

  public alternatives: Alternative[] = new Array<Alternative>();
  private comparators: Map<Criteria, Comparator<Alternative>>
    = new Map<Criteria, Comparator<Alternative>>();

  constructor(private criteriaService: CriteriaService) {
    // const a = this.addAlternative('Alternative A');
    // const b = this.addAlternative('Alternative B');
    // const k = this.addAlternative('Alternative K');

    // this.criteriaService.criterias.forEach((c: Criteria) => {
    //   if (c.name.indexOf('Price') >= 0) {
    //     this.setComparison(c, a, b, 7);
    //     this.setComparison(c, a, k, 3);
    //     this.setComparison(c, b, k, 3);
    //     return;
    //   }
    //   if (c.name.indexOf('Time') >= 0) {
    //     this.setComparison(c, a, b, -7);
    //     this.setComparison(c, a, k, -5);
    //     this.setComparison(c, b, k, 3);
    //     return;
    //   }
    //   if (c.name.indexOf('people') >= 0) {
    //     this.setComparison(c, a, b, 5);
    //     this.setComparison(c, a, k, 5);
    //     this.setComparison(c, b, k, -5);
    //     return;
    //   }
    // });
  }

  public addAlternative(name: string): Alternative {
    const a: Alternative = {name: name};
    this.alternatives.push(a);

    this.criteriaService.criterias.forEach((c: Criteria) => {
      this.alternatives.forEach((a2: Alternative) => {
        this.setComparison(c, a, a2, 1);
      });
    });
    return a;
  }

  public deleteAlternative(a: Alternative): void {
    this.alternatives.forEach((v: Alternative, i: number) => {
      if (v === a) {
        this.alternatives.splice(i, 1);
        return;
      }
    });

    this.criteriaService.criterias.forEach((c: Criteria) => {
      this.comparator(c).purge(a);
    });
  }

  public getComparisons(c: Criteria): Map<Alternative, Map<Alternative, number>> {
    return this.comparator(c).comparisons;
  }

  public setComparison(c: Criteria, a1: Alternative, a2: Alternative, value: number): void {
    this.comparator(c).setComparison(a1, a2, value);
  }

  public isDataCorrect(c: Criteria): boolean {
    return this.comparator(c).isTransitive();
  }

  private comparator(c: Criteria): Comparator<Alternative> {
    if (!this.comparators.has(c)) {
      this.comparators.set(c, new Comparator<Alternative>());
    }
    return this.comparators.get(c);
  }

  public rate(): Map<Alternative, number> {
    const res = new Map<Alternative, number>();
    const criteriaWeights = this.criteriaService.getWeights();

    this.alternatives.forEach((a: Alternative) => {
      let weight = 0;
      criteriaWeights.forEach((cw: number, c: Criteria) => {
        weight += cw * this.comparator(c).getWeights().get(a);
      });
      res.set(a, weight);
    });

    return res;
  }
}
