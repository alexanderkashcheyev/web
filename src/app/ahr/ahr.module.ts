import { AlternativeService } from './alternative.service';
import { SharedModule } from './../shared/shared.module';
import { CriteriaService } from './criteria.service';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CriteriaListComponent } from './criteria-list/criteria-list.component';
import { AlternativeListComponent } from './alternative-list/alternative-list.component';
import { AlternativeComparisonComponent } from './alternative-comparison/alternative-comparison.component';
import { ResultComponent } from './result/result.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule
  ],
  declarations:
  [
    CriteriaListComponent,
    AlternativeListComponent,
    AlternativeComparisonComponent,
    ResultComponent
  ],
  exports:
  [
    CriteriaListComponent,
    AlternativeListComponent,
    AlternativeComparisonComponent,
    ResultComponent
  ],
  providers:
  [
    CriteriaService,
    AlternativeService
  ]
})
export class AhrModule { }
