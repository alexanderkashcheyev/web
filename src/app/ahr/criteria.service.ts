import { Comparator } from './../shared/comparator';
import { Criteria } from './criteria';
import { Injectable } from '@angular/core';

@Injectable()
export class CriteriaService {

  public criterias: Criteria[] = new Array<Criteria>();
  private comparator: Comparator<Criteria> = new Comparator<Criteria>();

  constructor() {
    // const price = this.addCriteria('Price');
    // const time = this.addCriteria('Time to city center');
    // const people = this.addCriteria('Amount of bothered people');

    // this.setComparison(price, time, 5);
    // this.setComparison(price, people, 3);
    // this.setComparison(time, people, 3);
  }

  public addCriteria(name: string): Criteria {
    const c = new Criteria();
    c.name = name;
    this.criterias.push(c);

    this.criterias.forEach((v: Criteria) => {
      this.comparator.setComparison(c, v, 1, (c !== v));
    });
    return c;
  }

  public deleteCriteria(c: Criteria): void {
    this.criterias.forEach((value: Criteria, key: number) => {
      if (value === c) {
        this.criterias.splice(key, 1);
      }
    });

    this.comparator.purge(c);
  }

  public isTransitive(): boolean {
    return this.comparator.isTransitive();
  }

  public getComparisons(): Map<Criteria, Map<Criteria, number>> {
    return this.comparator.comparisons;
  }

  public setComparison(one: Criteria, two: Criteria, value: number): void {
    this.comparator.setComparison(one, two, value);
  }

  public getWeights(): Map<Criteria, number> {
    return this.comparator.getWeights();
  }
}
