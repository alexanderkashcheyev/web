import { Alternative } from './../alternative';
export class RatedResult {
    alternative: Alternative;
    rate: number;
}
