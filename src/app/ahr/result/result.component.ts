import { RatedResult } from './ratedResult';
import { AlternativeService } from './../alternative.service';
import { Alternative } from './../alternative';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'eahr-result',
  templateUrl: './result.component.html'
})
export class ResultComponent implements OnInit {

  constructor(private alternativeService: AlternativeService) { }

  ngOnInit() {
  }

  rate(): RatedResult[] {
    const unsorted = this.alternativeService.rate();
    const res = new Array<RatedResult>();

    unsorted.forEach((r: number, a: Alternative) => {
      const currRate: RatedResult = {alternative: a, rate: r};

      if (res.length === 0) {
        res.push(currRate);
      } else {
        res.forEach((v: RatedResult, i: number) => {
          if (v.rate < currRate.rate) {
            res.splice(i, 0, currRate); // put the new one before
            return;
          } else if (i === res.length - 1) {
            res.push(currRate); // put the new one to the end;
            return;
          }
        });
      }
    });

    return res;
  }

  alternatives(): Alternative[] {
    return this.alternativeService.alternatives;
  }

}
